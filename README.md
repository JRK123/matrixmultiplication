#DATA_STRUCTURE IMPLEMENTATION

1) This program calculates the time for matrix multiplication by varying order
for i, j, k in three for loops.

2) Random file is provided to generate matrix for file1 and file2 for		   
multiplication. 
syntax is : 
$ ./random >> file1 
100 1000 // 100 X 100 matrix having maximum number as 1000

3) To execute code : 
$ ./strtok Order_file_name file1_matrix1 file2_matrix2 answer
    - to compile strtok.cpp do "g++ strtok.cpp -o strtok"

4) Order_file_name has order of square matrix
    - file1 has the matrix1
    - file2 has the matrix2

5) if required answer will be stored in answer file.
